from django.shortcuts import render
from projects.forms import ProjectCreateForm
from projects.models import Project
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect


@login_required
def list_projects(request):
    projects = Project.objects.filter(members=request.user)
    context = {"projects": projects}

    return render(request, "projects/list.html", context)


@login_required
def show_project(request, pk):
    project = Project.objects.get(pk=pk)
    context = {"project": project}

    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectCreateForm(request.POST or None)
        if form.is_valid():
            project = form.save()
            return redirect("show_project", pk=project.pk)
    else:
        form = ProjectCreateForm()
    context = {"form": form}

    return render(request, "projects/create.html", context)
