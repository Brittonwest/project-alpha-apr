from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from tasks.models import Task
from tasks.forms import TaskCreateForm, TaskUpdateView
from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskCreateForm(request.POST or None)
        if form.is_valid():
            project = form.save()
            return redirect("show_project", pk=project.pk)
    else:
        form = TaskCreateForm()
    context = {"form": form}

    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}

    return render(request, "tasks/list.html", context)


class TaskUpdateView(UpdateView):
    model = Task
    template_name = "tasks/list.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
