
 *** References [https://learn-2.galvanize.com/cohorts/3352/blocks/1847/content_files/build/01-practice-and-project/66-assessment-project.md] *** 
* [x] Feature 1	- Install dependencies
  * [x] Fork / Copy Project Alpha
  * [x] Open Terminal
    * [x] cd projects
    * [x] git clone --
    * [x] cd project-alpha-apr
    * [x] code .
 * [x] Activate Venv
 * [x] python -m pip install --upgrade pip
 * [x] pip install django
 * [x] pip install black
 * [x] pip install flake8
 * [x] pip install djhtml
 * [x] deactivate
 * [x] activate
 * [x] pip freeze > requirements.txt
 * [x] *** [Test-Feature-1]  ***
 * [x] *** [5-Tests-No-Issues] ***
 * [x] git add / commit / push
* [x] Feature 2 - Set up the Django project and apps [Depends-on-1]
  * [x] Create Django project name Tracker
    * [x] django-admin startproject tracker .
  * [x] Create Django app named accounts
    * [x] python manage.py startapp accounts
  * [x] Create Django app named projects
    * [x] python manage.py startapp projects
  * [x] Create Django app named tasks
    * [x] python manage.py startapp tasks
  * [x] Run migrations
    * [x] python manage.py makemigrations
    * [x] python manage.py migrate
  * [x] Create a superuser
    * [x] python manage.py createsuperuser
  * [x] *** [Test-Feature-2] ***
  * [x] *** [7-Tests-3-ERRORS] ***
    * [x] [ADD]tracker / settings.py
      * [x] [ADD]installed apps =
        * [x] [ADD]'accounts.apps.AccountsConfig',
        * [x] [ADD]'projects.apps.ProjectsConfig',
        * [x] [ADD]'tasks.apps.TasksConfig',
          * [x] [ADD]save
  * [x] *** [Test-Feature-2] ***
  * [x] *** [7-Tests-no-errors] ***
  * [x] git add / commit / push
* [x] Feature 3 - The Project model	[Depends-on-2]
  * [x] Create Project Model in projects
  * [x] from django.conf import settings
  * [x] USER_MODEL = settings.AUTH_USER_MODEL
    * [x] projects / models.py
      * [x] Class Project(models.Model):
        * [x] name = models.CharField(max_length=200)
        * [x] description = models.TextField()
        * [x] members = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="projects")
      * [x] def __str__(self):
        * [x] return self.name
  * [x] python manage.py makemigrations
    * [x] [ERROR-CharFields-must-be-defined]
      * [x] models.TextField() > CharField
  * [x] python manage.py migrate
  * [x] *** [Test-Feature-3] ***
  * [x] *** [12-Tests-no-errors] ***
  * [x] git add / commit / push
* [x] Feature 4 - Registering Project in the admin	[Depends-on-3]
  * [x] Register Project w/ Admin.py
    * [x] from projects.models import Project
    * [x] admin.site.register(Project)
  * [x] *** [Test-Feature-4] ***
  * [x] *** [1-test-no-errors] ***
  * [x] git add / commit / push
* [x] Feature 5	- The Project list view	[Depends-on-3]
  * [x] Create ProjectListView in views.py 
  * [x] from projects.models import Project
  * [x] from django.shortcuts import render, redirect
    * [x] def list_projects(request):
    * [x] projects = Project.objects.all()
    * [x] context = {"projects": projects}
    * [x] return render(request, "projects/list.html", context)
  * [x] Register view in projects
    * [x] create projects/urls.py
    * [x] from django.urls import path
    * [x] import projects.views
    * [x] path ""
    * [x] name list_projects
  * [x] Register URL in Tracker
    * [x] import include
    * [x] path "projects/" include("projects.urls"),
  * [x] create template for list view
    * [x] create templates
    * [x] create projects
      * [x] list.html
      * [x] html:5
        * [x] `<main>`
          * [x] `<div>`
            * [x] `<h1>My Projects</h1>`
            * [x] if statement
            * [x] [if-no-projects] `<p>You are not assigned to any projects</p>`
            * [x] tables 2 columns
              * [x] `<table>`
                * [x] `<thead>`
                  * [x] `<tr>`
                    * [x] `<th>Name`
                    * [x] `<th>Number of Tasks`
  * [x] *** [Test-Feature-5] ***
  * [x] *** [9-Tests-1-Failure] ***
    * [ ] [FAILURE] Reponse did not have [p-tag] as child of [div]
  * [x] git add / commit / push
* [x] Feature 6	- Default path redirect	[Depends-on-5]
  * [x] in tracker urls.py 
  * [x] import reverse_lazy
  * [x] from django.views.generic.base import RedirectViews
    * [x] RedirectView from "" -> "home"
  * [x] *** [Test-Feature-6] ***
  * [x] *** [2-Tests-No-issues] ***
* [x] git add / commit / push
* [x] Feature 7 - Login page [Depends-on-2]
  * [x] Register LoginView in accounts urls.py
    * [x] create urls.py
    * [x] from django.urls import path
    * [x] from django.contrib.auth import views as auth_views
    * [x] path "login/" auth_views.LoginView.as_view() name login
  * [x] in tracker settings.py
    * [x] LOGIN_REDIRECT_URL = "home"
  * [x] in urls.py tracker project
    * [x] create path "accounts/"
  * [x] create templates in accounts
  * [x] create registration in templates/accounts
    * [x] create login.html
      * [x] create POST form in login.html
        * [x] include html:5
          * [x] `<main>`
            * [x] `<div>`
              * [x] `<h1>Login</h1>`
              * [x] `<form> method="POST"
  * [x] in tracker settings.py
    * [x] create LOGIN_REDIRECT_URL [finished-above]
  * [x] *** [Test-Feauture-7] ***
  * [x] *** [10-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] Feature 8 - Require login for project list view [Depends-on-5/7]
  * [x] set Project as login required
    * [x] in projects.views
    * [x] from django.contrib.auth.decorators import login_required
    * [x] on projects @login_required
  * [x] change query-set on projects
    * [x] Project.objects.all -> Project.objects.filter(members=request.user)
  * [x] *** [Test-Feature-8] ***
  * [x] *** [3-Tests-1-ERROR] ***
    * [x] [Failure] test_projects_list_shows_no_member_projects_when_none_exist
* [BREAKPOINT-MONDAY-NIGHT-50-TESTS-1-ERRORS-1-FAILURE] 
* [x] Feature 9 - Logout page [Depends-on-7]
  * [x] import LogoutView
    * [x] register LogoutView in urlpatterns
  * [x] in tracker settings.py create set and LOGOUT_REDIRECT_URL = "login"
  * [x] *** [Test-Feature-9] ***
  * [x] *** [4-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] Feature 10 - Sign up page [Depends-on-2]
  * [x] import UserCreationForm
    * [x] from django.contrib.auth.forms import UserCreationForm
  * [x] Use Create_user method to create a new user
  * [x] use login function that logs an account in
  * [x] redirect user to the path registered with "home"
  * [x] create html accounts/templates/registration/signup.html
    * [x] method=post FORM in HTML
      * [x] `<main>`
        * [x] `<div>`
          * [x] `<h1>Signup</h1>`
            * [x] `<form method="POST">`
              * [x] {% csrf_token %}
              * [x] {{ form.as_p }}
              * [x] `<button type="submit">Signup</button>`
  * [x] *** [Test-Feature-10] ***
  * [x] *** [10-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] Feature 11 - Task Model [Depends-on-3]
  * [x] Create Task Model -> tasks models.py app
  * [x] import settings
  * [x] import CASCADE
  * [x] from django.db.models import CASCADE
  * [x] USER_MODEL = settings.AUTH_USER_MODEL
      * [x] Class Task(models.Model):
        * [x] name = models.CharField(max_length=200)
        * [x] start_date = models.DateTime()
        * [x] due_date = models.DateTime
        * [x] is_completed = models.BooleanField(default=False)
        * [x] project = models.ForeignKey(projects.Project, related_name="tasks", on_delete=CASCADE)
        * [x] assignee = models.ForeginKey(settings.AUTH_USER_MODEL, null=True, related_name="tasks", on_delete=models.SET_NULL)
          * [x] def __str__(self):
            * [x] return render self.name
  * [x] python manage.py makemigrations
  * [x] python manage.py migrate
  * [x] *** [Test-Feature-11] ***
  * [x] *** [20-Tests-4-failures] ***
  * [x] [FAILURE] -> task_model_has_assignee_foreign_key_field -> AttributeError: type object 'Task' has no attribute 'assignee'
  * [x] [FAILURE] -> test_task_model_has_assignee_has_on_delete_set_null -> AttributeError: type object 'Task' has no attribute 'assignee'
  * [x] [FAILURE] -> test_task_model_has_assignee_related_name_of_tasks -> AttributeError: type object 'Task' has no attribute 'assignee'
  * [x] [FAILURE] -> test_task_model_has_assignee_related_to_auth_user -> AttributeError: type object 'Task' has no attribute 'assignee'
  * [x] [ADD] assignee to model
  * [x] *** [Re-Test-Feature-11] ***
  * [x] *** [20-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] Feature 12 - Registering Task in admin [Depends-on-11]
  * [x] register Task model in admin.py
    * [x] from django.contrib import admin
    * [x] from tasks.models import Task
    * [x] admin.site.register(Task)
  * [x] *** [Test-Feature-12] ***
  * [x] *** [1-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] Feature 13 - Project Detail View [Depends-on-12]
  * [x] Create view for project Details
  * [x] User must be logged in
    * [x] @login_required
  * [x] in projects.urls
    * [x] register view `"<int:pk/>"` name="show_project"
  * [x] Create Template to show project details & table of tasks
  * [x] Update list template to show number of tasks for a project
    * [x] `<td>{{ project.tasks.all| length}}</td>`
  * [x] Update list template to have link from project.name -> detail view
    * [x] `<td><a href={% url "show_project" project.pk %}>{{ project.name }}</a></td>`
  * [x] projects/templates/projects/detail.html
    * [x] html:5
      * [x] `<main>`
        * [x] `<div>`
          * [x] `<h1>{{ project.name }}</h1>`
          * [x] `<p>{{ project.description }}</p>`
          * [x] `<h2>Tasks</h2>`
          * [ ] `{% if project.tasks.all %}`
            * [x] `<table>`
              * [x] `<thead>`
                * [x] `<tr>`
                  * [x] `<th>Name</th>`
                  * [x] `<th>Assignee</th>`
                  * [x] `<th>Start date</th>`
                  * [x] `<th>Due date</th>`
                  * [x] `<th>is completed</th>`
                * [x] `<tr>`
              * [x] `<thead>`
            * [x] `<tbody>`
              * [x] {% for task in Project.tasks.all %}
                * [x] `<td>{{ task.name }}</tr>`
                * [x] `<td>{{ task.assignee }}</tr>`
                * [x] `<td>{{ task.start_date }}</tr>`
                * [x] `<td>{{ task.due_date }}</tr>`
                * [x] `<td>{{ task.is_completed }}</tr>` [yes or no response]
              * [x] otherwise -> This project has no tasks
  * [x] *** [Test-Feature-13] ***
  * [x] *** [15-Tests-2-FAILURES] ***
  * [x] [FAILURE] test_project_detail_with_a_tasks_shows_is_completed_header -> Did not find the header 'Is completed' in the detail page
  * [x] [FAILURE] test_project_list_has_number_of_tasks -> '1' not found in 'DocumentMy ProjectsNameNumber of tasksZZZZZZ' : Did not find the number of tasks for the project in the page
    * [x] [changed] is completed -> Is completed
    * [x] [changed] project.tasks.all -> project.tasks.all| length
  * [x] *** [Re-Test-Feature-13] ***
  * [x] *** [15-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] Feature 14 - Project Create View [Depends-on-2]
  * [ ] Create a create view for Project
  * [x] [create-forms.py] 
  * [x] from django import forms
  * [x] from projects.models import Project
  * [x] class ProjectCreateForm(forms.ModelForm):
    * [x] class Meta:
      * [x] model = Project
      * [x] fields = [name, description, members]
    * [x] show name
    * [x] show description
    * [x] show members
    * [x] all in form handle
    * [x] must be @login_required
    * [x] in views import redirect
    * [ ] redirect form.save -> show_project
  * [x] Register view in projects.urls
    * [x] path"create/" -> name"create_project"
  * [x] add link to create in list html
    * [x] `<p>`
      * [x] `<a href="{% url 'create_project' %}>Create a new project</a>`
  * [ ] Create HTML
    * [x] html:5
        * [x] `<main class="create-form">`
          * [x] `<div>`
            * [x] `<h1>Create a new project</h1>`
            * [x] `<form method="POST">`
              * [x] `{% csrf_token %}`
              * [x] `{{ form.as_p }}`
              * [x] `<button type="submit">Create</button>`
            * [x] `</form>
  * [x] *** [Test-Feature-14] ***
  * [x] *** [11-Test-1-Failure] ***
  * [x] [FAILURE] test_create_redirects_to_detail -> Create does not redirect to detail
  * [x] [ADD] project = form.save -> redirect("show_project", pk=project.pk)
  * [x] *** [Re-Test-Feature-14] ***
  * [x] *** [11-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] Feature 15 - Task Create View [Depeonds-on-11]
  * [x] Create a Create view for Task
  * [x] impost redirect, login_required, .models Task, TaskCreateForm
    * [x] all properties except is_completed
    * [x] @login_required
    * [x] redirects detail page of that project
  * [x] Register view in tasks app 
    * [x] create tasks/urls.py
    * [x] path("create"/) name create_task
  * [x] include url patterns in tracker project
    * [x] prefix "tasks/"
  * [x] create template
  * [x] Add Create Task link in project detail page
  * [x] *** [Test-Feature-15] ***
  * [x] *** [13-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] Feature 16 - Show "My Tasks" in List View [Depends-on-11]
  * [x] Create a list view for task
    * [x] only person sees tasks asigned to them by filtering task.assignee = request.user
    * [x] @login_required
  * [x] Register view in task app for path 
    * [x] path "mine/" - name"show_my_tasks"
  * [x] Create HTML - [mirror-project-list]
  * [x] *** [Test-Feature-16] ***
  * [x] *** [7-Tests-No-Issues] ***
  * [x] git add / commit / push
* [x] [BREAKPOINT-TUESDAY-LUNCH-FULL-144-TESTS-5-ERRORS-3-FAILURES-[17-19-NOT-STARTED]
* [x] Feature 17 - Completing a task [Depends-on-16]
  * [x] Create update view for Task 
    * [x] only concern is with is_completed field
  * [x] When view submits it redirects to show_my_tasks URL path
    * [x] redirect my tasks view - success_url property
  * [x] Register view in tasks app 
    * [x] `"<int:pk>/complete/"` and name "complete_task -> tasks urls.py
  * [x] [no-template-view]
  * [ ] modify tasks view to comply with template
  * [ ] *** [Test-Feature-17] *** 
  * [ ] *** [11-Tests-1-Failure] ***
  * [ ] [FAILURE] test_my_tasks_with_one_task_has_a_button -> AssertionError: 0 != 1 : Found more than one button
  * [ ] [FAILURE-UPDATE] Not sure how to format HTML. Will come back too.
* [x] Feature 18 - Markdownify [Depends-on-17]
  * [x] Install django-markdownify using pip [refer-to-instructions]
  * [x] Put it in INSTALLED_APPS in tracker settings.py
    * [x] "markdownify.apps.MarkdownifyConfig",
      * [x] disable sanitation feature in ^
        * [x] MARKDOWNIFY = {"default": {"BLEACH": False}}
  * [x] in your template for Project Detail View
    * [x] load markdownify template
      * [x] replace p tag and {{ project.desciption }} in project detail view with
        * [x] {{ project.description|markdownify }}
  * [x] pip freeze > requirements.txt
  * [x] *** [Test-Feature-18] ***
  * [x] *** [1-Test-No-Issues] ***
* [x] Feature 19 - Navigation [Depends-on-5/7/9/10/16]
* [ ] *** [TEST] ***
* [ ] *** [144-Tests] ***
* [ ] *** [2-FAILURES] ***
* [ ] *** [2-ERRORS] ***

